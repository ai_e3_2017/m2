"""
Raw relation extraction
Input: XML with a tagged corpus that has as tags:
    entity type, POS
Output: name of the relation between the central concepts
        based on the verb defining the possible relation
"""
import xml.etree.ElementTree as ET
import input_tags_name, utility_constants, xml_util
import output_formatter, ml_module
from score_relations import compute_score

def identify_anaphora(tree, concept):
    """
    Extension for the relation extraction
    :param concept: Central concept for which the relation is searched
    :return: A list of the taggs containing references to the exactly
            same concept in the same tree
    """
    pass

def get_sentence_containing_node(tag, root):
    try:
        for sententce in root.iter(input_tags_name.SENTENCE):
            if tag in sententce.findall("*"):
                return sententce
        return None
    except AttributeError:
        raise AttributeError

def get_next_verb(sentence, entity):
    try:
        found_entity = False
        for node in sentence.iter():
            if node == entity:
                found_entity = True
            if found_entity:
                list = xml_util.get_all_children_having_attribute(sentence, input_tags_name.POS, input_tags_name.VERB)
                # find all children of sentence tagged with POS = VERB
                if len(list)>0:
                    return list[0]
        return
    except AttributeError:
        raise AttributeError

def find_next_noun_phrase(sentence, entity):
    found_entity = False
    try:
        for node in sentence.iter():
            if node == entity:
                found_entity = True
            if found_entity and node.tag == input_tags_name.NOUN_PHRASE:
                return node
    except AttributeError:
        raise AttributeError

def extract_verb_based_relation(xml_file):
    result_json = {}
    tree = ET.parse(xml_file)
    root = tree.getroot()
    for entity in root.iter(input_tags_name.NOUN_PHRASE):
        concept = xml_util.get_content_of_all_inner_nodes(entity)
        if concept not in result_json:
            result_json[concept] = {}
        try:
            sentence = get_sentence_containing_node(entity, root)
            # determine the relation name based on the following verb
            verb = get_next_verb(sentence, entity)

            relation_type = verb.text.strip()
            # add it to the result json
            if relation_type not in result_json[concept]:
                result_json[concept][relation_type] = {}

            correlated_noun_phrase = find_next_noun_phrase(sentence, verb)
            correlated_noun_phrase_text = xml_util.get_content_of_all_inner_nodes(correlated_noun_phrase)

            # add the confidence value associated to the identified pair
            if correlated_noun_phrase.text not in result_json[concept][relation_type]:
                result_json[concept][relation_type] = {}
                result_json[concept][relation_type][correlated_noun_phrase_text] = \
                    ml_module.confidence_of_relation(entity, correlated_noun_phrase)
            else:
                # found multiple times, improve confindence by a percent
                init_confidence = result_json[entity][relation_type][correlated_noun_phrase].text
                result_json[entity][relation_type][correlated_noun_phrase] += \
                    init_confidence + \
                    utility_constants.CONFIDENCE_IMPROVEMENT_PERCENT * init_confidence
        except AttributeError:
            pass
            # print("Attribute error, node element not found")
    return result_json

def extract_relation_type_between_concepts(xml_file, doc, corpus="../med_articles"):
    """
    :param xml_file: Tagged text as XML format,
                    containing the "concept" tag,
                    the content being the value of the concept
                    eg. <concept name="medical_disease">Cancer</concept>
    :return: JSON format containing:
                entity_name: {
                    relation_type: {
                        entity1 : "probability_that_gives_the_confidence";
                        entity2 : "probailitiy2";
                        ...
                        entityn: "probabilityn";
                        }
                    }
                }
    """
    result_json = extract_verb_based_relation(xml_file)
    # TODO other types of relations to be identified and added here
    # TODO merge results of those methods
    result_json = output_formatter.remove_redundancies(result_json)
    return compute_score(result_json, doc, corpus)


if __name__ == "__main__":
    print(extract_relation_type_between_concepts("input_from_m1.xml", doc="input.txt"))