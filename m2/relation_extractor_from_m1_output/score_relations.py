import os
import PyPDF2
import textract
from textblob import TextBlob
import tf_idf

def compute_score(json_relations, doc, corpus_folder):
    scores = tf_idf.get_tf_idf_for_file_in_folder(doc, corpus_folder)
    for relation in json_relations:
        words = relation.split()
        for word in words:
            if scores[word]:
                # if there is a score computed with tf-idf for this word
                # add the score to the total score of the relation
                # containing that word
                if not json_relations[relation]: #no score yet
                    json_relations[relation] = scores[word]
                else:
                    json_relations[relation] += scores[word]
    return json_relations