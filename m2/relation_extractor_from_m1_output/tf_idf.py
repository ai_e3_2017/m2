import os
import numpy
from textblob import TextBlob
import PyPDF2
import textract


def tf(word, blob):
    return blob.words.count(word) / len(blob.words)


def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob.words)


def idf(word, bloblist):
    return numpy.log(len(bloblist) / (1 + n_containing(word, bloblist)))


def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

bloblist = []
def get_words_for_blob(blob):
    scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)

    return sorted_words

# GET TEXT FROM FILE
def get_words(folder_name, filename):
    actual_path = os.path.join(folder_name, filename)
    if filename.endswith('.pdf'):
        pdfFileObj = open(actual_path, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        num_pages = pdfReader.numPages
        count = 0
        text = ""
        while count < num_pages:
            pageObj = pdfReader.getPage(count)
            count += 1
            text += pageObj.extractText()
        if text != "":
            text = text
    elif filename.endswith('.txt'):
        f = open(actual_path, 'r')
        text = f.read()
        f.close()
        print(text)

    return text

def get_tf_idf_for_folder(folder_name):
    all_scores = []
    for item in os.listdir(folder_name):
        filename = item
        bloblist.append(TextBlob(get_words(folder_name, filename)))

    for i, blob in enumerate(bloblist):
        # print("Top words in document {}".format(i + 1))
        scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        # print(sorted_words)
        all_scores.append(sorted_words)
    return all_scores
#    for word, score in sorted_words[:3]:
        #print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))

default_folder = "../med_articles"

# get_tf_idf_for_folder(default_folder)
