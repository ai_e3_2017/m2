let fs = require('fs'),
  PDFParser = require("pdf2json");

let pdfParser = new PDFParser();

let targetFilePath = process.argv[2]

pdfParser.on("pdfParser_dataError", errData => console.error(errData.parserError) );
pdfParser.on("pdfParser_dataReady", pdfData => {
  currentPhrase = ''
  inProgress = false

  allTitles =[]

  pdfData.formImage.Pages.forEach(page => {
    page.Texts.forEach(word => {
      let wordText = word.R ? word.R[0].T : {}

      if(word.sw == 0.365875) {
        if(inProgress) {
          currentPhrase = currentPhrase + ' ' + wordText
        } else {
          currentPhrase.length < 140 ? allTitles.push(currentPhrase) : {}
          console.log(word)

          currentPhrase = wordText

          inProgress = true
        }
      } else {
        inProgress = false
      }
    })
  })
  fs.writeFile("./pdf2json/med1_hepatoma.json", JSON.stringify(pdfData));
  fs.writeFile("./pdf2json/med1_hepatoma.titles.txt", JSON.stringify(allTitles));

  return "./pdf2json/med1_hepatoma.titles.txt"
});

//pdfParser.loadPDF("./med_articles/med1_hepatoma.pdf");
if(!targetFilePath) { targetFilePath = "./med_articles/med1_hepatoma.pdf" }
pdfParser.loadPDF(targetFilePath);

//  ./med_articles/med1_hepatoma.pdf
